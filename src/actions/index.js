export function selectMovie(movie) {
    //returns object action
    return {
        type: "MOVIE_SELECTED",
        payload: movie
    }
}

//this action creator is called in container on user event triggered