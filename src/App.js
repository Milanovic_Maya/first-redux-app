import React, { Component } from 'react';
import MovieList from './containers/MovieList';
import MovieDetails from "./containers/MovieDetails";

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <MovieList />
          <MovieDetails />
        </div>
      </div>
    );
  }
}

export default App;
