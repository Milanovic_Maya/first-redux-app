import React, { Component } from "react";
import { connect } from "react-redux";
import { selectMovie } from "../actions/index";
import { bindActionCreators } from "redux";


class MovieList extends Component {

    render() {
        return (
            <div className="col-sm-4">
                <div class="jumbotron">
                    <h1 class="display-4">List of Movies to be aired:</h1>
                    <p class="lead">
                        Movie list will be eventually expanded.
                    </p>
                </div>
                <ul className="list-group">
                    {this.props.movies.map(movie => {
                        return <li
                            key={Math.random() * 100}
                            className="list-group-item text-center"
                            onClick={() => {
                                this.props.selectMovie(movie)
                            }}>
                            {`Title: ${movie.title}, Year: ${movie.year}`}
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        movies: state.movies
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ selectMovie: selectMovie }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieList);