import React, { Component } from "react";
import { connect } from "react-redux";

class MovieDetails extends Component {

    render() {
        if (!this.props.movie) {
            return <div className="card col-sm-8">
                <div className="card-body lead">
                    Please, select a movie from the list.
            </div>
            </div>
        }

        return (
            <div className="card col-sm-8">
                <div className="card-body">
                    <div className="card-header">{this.props.movie.title}</div>
                    <p>{this.props.movie.synopsis}
                    </p>
                    <p>
                        Year:
                    {this.props.movie.year}
                    </p>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    //must RETURN OBJECT THAT IS PIECE OF STATE
    //that object is going to be mapped into props
    return { movie: state.activeMovie }
}


export default connect(mapStateToProps)(MovieDetails);
