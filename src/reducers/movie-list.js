export default function () {
    return [{
            title: "The Third Shadow",
            year: 1963,
            synopsis: "In the mountain regions of Hida, the dreams of a peasant named Kyonosuke, who longed to be a samurai, come true when he becomes one of three doubles, of shadows, of Lord Yasutaka. After months of intense and cruel training, he faces his destiny when the Lord and the other two shadows are killed in battle and he must take on the role of Lord Yasutaka…."
        },
        {
            title: "The Betrayal",
            year: 1966,
            synopsis: "Based on Buntarô Futagawa’s 1925 film Orochi (The Serpent),The Betrayal stars popular Japanese actor Raizô Ichikawa."
        },
        {
            title: "Yoso",
            year: 1963,
            synopsis: "Yoso is truly a lost classic, set in the Nara Era (710-794), from Kinugasa Teinosuke, starring Raizo Ichikawa."
        }
    ]
}