//need to combine reducers into one store

import { combineReducers } from "redux";

//import our reducers

import MovieListReducer from "./movie-list";
import ActiveMovie from "./active-movie";

const rootReducer = combineReducers({
    //our app state will have key named movies
    //whose value is managed by reducer MovieListReducer
    movies: MovieListReducer,
    activeMovie: ActiveMovie
});

export default rootReducer;